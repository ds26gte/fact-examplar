provide {fact: fact} end
provide-types *

fun fact(n :: Number) -> Number:
  2 * n
end

