provide {fact: fact} end
provide-types *

fun fact(n :: Number) -> Number:
  n + 1
end

